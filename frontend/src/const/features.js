export default Object.freeze({
    installation: 'installation',
    scalability: 'scalability',
    availability: 'availability',
    loadBalancing: 'load balancing',
    networking: 'networking',
    updates: 'updates',
})