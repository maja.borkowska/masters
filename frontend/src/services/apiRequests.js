import axios from 'axios'
import { SWARM_SERVICE_URL, K8S_SERVICE_URL } from './configService'

export function getSwarmFeature(feature) {
    return axios.get(`${SWARM_SERVICE_URL}${feature}`)
}

export function getK8sFeature(feature) {
    return axios.get(`${K8S_SERVICE_URL}${feature}`)
}