export const SWARM_SERVICE_URL = process.env.VUE_APP_SWARM_SERVICE_URL || 'http://localhost:3000/'
export const K8S_SERVICE_URL = process.env.VUE_APP_K8S_SERVICE_URL || 'http://localhost:4000/'