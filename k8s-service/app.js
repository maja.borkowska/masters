import express from 'express'
import cors from 'cors'
import features from "./src/features.js";
const port = 4000

const app = express()
app.use(cors())

app.get('/', (req, res) => {
    res.send('Hello from k8s service!')
})

Object.keys(features).forEach((feature) => {
    app.get(`/${feature}`, (req, res) => {
        res.send(features[feature])
    })
})

app.listen(port, () => {
    console.log(`K8s service listening at http://localhost:${port}`)
})